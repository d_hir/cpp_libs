////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  CAUCHY.H
////  ========
////
////  HEADER FILE FOR APPLYING THE CAUCHY THEOREM TO GET FUNCTION VALUES IN
////  THE COMPLEX PLANE INSIDE A CONTOUR ON WHICH THE FUNCTION IS KNOWN.
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef CAUCHY_H
#define CAUCHY_H

/* STANDARD DATA TYPE DEALING WITH COMPLEX NUMBERS */
#include <complex>

/* THE CAUCHY THEOREM UTILIZES A CONTOUR ON WHICH THE FUNCTION IS KNOWN */
#include "contour.h"
/* IN THE CAUCHY THEOREM, THE FUNCTION GETS INTEGRATED OVER THE CONTOUR */
#include "integrator.h"

/* TO BE ABLE TO USE THE FOLLOWING FUNCTIONS WITHOUT 'STD::' PREFIX */
using std::abs;
using std::sqrt;

////////////////////////////////////////////////////////////////////////////////
// 
// CAUCHY CLASS
// ============
//
// MAIN CLASS OF THIS MODULE. CAN BE USED TO GET THE VALUE OF A FUNCTION INSIDE
// A CLOSED CONTOUR BY SUPPLYING THE FUNCTION ON THIS CONTOUR.
// 
////////////////////////////////////////////////////////////////////////////////
template<typename O = double, typename C = std::complex<O>>
class Cauchy {
    private:
        /* THE CAUCHY THEOREM NEEDS A CONTOUR TO INTEGRATE OVER */
        const Contour<O, C> c;
        /* AN INTEGRATOR IS NEEDED TO INTEGRATE OVER THE CONTOUR */
        Integrator<O, C> i;
        /* THIS IS A FUNCTION THAT IS CONSTANTLY 1 EVERYWHERE (f(z) = 1) */
        const ContourFunction<O, C> unit_function;

    public:
        ////////////////////////////////////////////////////////////////////////
        // CAUCHY::CAUCHY
        // --------------
        // CONSTRUCTOR CREATING A CAUCHY OBJECT FROM A GIVEN CONTOUR.
        //
        // IN:
        //   c_ (CONTOUR<O, C>) ... CONTOUR TO BE USED IN CAUCHY THEOREM
        // OUT:
        //   (CAUCHY)
        ////////////////////////////////////////////////////////////////////////
        Cauchy(const Contour<O, C> c_) : 
                /* STORE CONTOUR LOCALLY AS MEMBER */
                c(c_), 
                /* CREATE INTEGRATOR USING GAUSS INTEGRATION IN [-1, 1] */
                i(IntegrationTechnique::RawGauss),
                /* CREATE FUNCTION f(z) = 1 TO BE USED IN NORMALIZATION */
                unit_function(c.num_points_vertical, c.num_points_parabola, 
                1.0) {
            /* SET INTEGRATOR TO USE GAUSS LEGENDRE INTEGRATION */
            i.setPolynomial(&double_polynomial.Legendre);
        }

        ////////////////////////////////////////////////////////////////////////
        // CAUCHY::_GETINNER
        // -----------------
        // INTERNAL METHOD TO GET FUNCTION INSIDE CONTOUR WITHOUT ANY CHECKS.
        //
        // IN:
        //   q_sqr (C) ... POINT INSIDE CONTOUR WHERE FUNCTION IS WANTED
        //   f (CONTOURFUNCTION<O, C>&) ... FUNCTION AT CONTOUR
        // OUT:
        //   (C) VALUE OF FUNCTION f AT POINT q_sqr
        ////////////////////////////////////////////////////////////////////////
        C _getInner(C q_sqr, const ContourFunction<O, C>& f) {
            /* INITIALIZE TEMPORARY SUMMING VARIABLE TO 0.0 */
            C sum(0.0);
            /* ITERATE OVER ALL CONTOUR SEGMENTS */
            for (int cp = 0; cp < 4; cp++) {
                /* SET INTEGRAND TO BE EQUAL TO f */
                std::vector<C> integrand = f[cp];
                /* SET INTEGRAND TO BE f(z(t)) * dz/dt / (z - q^2) */
                for (int j = 0; j < (int) integrand.size(); j++) {
                    integrand[j] *= c.segments[cp].zp[j];
                    integrand[j] /= (c.segments[cp].z[j] - q_sqr);
                }
                /* INTEGRATE INTEGRAND OVER ALL t AND SUM ALL SEGMENTS UP */
                sum += i.integrate(c.segments[cp].t, integrand);
            }
            /* RETURN SUM OF ALL SUBINTEGRALS ALONG THE SEGMENTS */
            return sum;
        }

        ////////////////////////////////////////////////////////////////////////
        // CAUCHY::GETINNER
        // ----------------
        // METHOD TO GET FUNCTION INSIDE CONTOUR.
        // CHECKS IF VALUE IS INSIDE CONTOUR. IF IT IS NOT, THEN RETURNS C(0.0)
        //
        // IN:
        //   q_sqr (C) ... POINT INSIDE CONTOUR WHERE FUNCTION IS WANTED
        //   f (CONTOURFUNCTION<O, C>&) ... FUNCTION AT CONTOUR
        // OUT:
        //   (C) VALUE OF FUNCTION f AT POINT q_sqr
        ////////////////////////////////////////////////////////////////////////
        C getInner(C q_sqr, const ContourFunction<O, C>& f) {
            /* INITIALIZE RETURN VALUE TO BE 0.0 AS COMPLEX NUMBER */
            C ret_val (0.0);
            /* IF IT IS INSIDE, CALCULATE IT. OTHERWISE LEAVE 0.0 IN ret_val */
            if (c.isInside(q_sqr)) {
                /* GET NORMALIZATION 2 * PI * i BY INTEGRATING OVER 1 */
                C norm = _getInner(q_sqr, unit_function);
                /* CHECK IF NORM IS ALMOST 0; IF IT IS, RETURN 1.0 */
                /* OTHERWISE CALCULATE INNER POINT AND APPLY NORMALIZATION */
                ret_val = abs(norm) > 1e-12 ? _getInner(q_sqr, f) / norm : 1.0;
            }
            /* IF IN DEBUG MODE, NOTE THAT POINT LIES OUTSIDE CONTOUR */
//#ifdef DEBUG_MODE
//            else
//                std::cout << "[W:CAUCHY] Point outside contour: " << 
//                        q_sqr << "\n";
//#endif
            /* RETURN VALUE INSIDE ret_val */
            return ret_val;
        }
        
#ifdef DEBUG_MODE
        ////////////////////////////////////////////////////////////////////////
        // CAUCHY::GETNORMALIZATION
        // ------------------------
        // DEBUG METHOD THAT RETURNS THE NORMALIZATION CONSTANT (2 * PI * i)
        // CALCULATED BY APPLYING CAUCHY THEOREM TO A CONSTANT FUNCTION f(z) = 1
        //
        // THE RETURN VALUE SHOULD BE ALMOST INDEPENDENT OF q_sqr
        //
        // IN:
        //   q_sqr (C) ... POINT INSIDE CONTOUR WHERE FUNCTION IS WANTED
        // OUT:
        //   (C) SHOULD EQUAL 2 * PI * i, INDEPENDENT OF ARGUMENT q_sqr
        ////////////////////////////////////////////////////////////////////////
        C getNormalization(C q_sqr) {
            return _getInner(q_sqr, unit_function);
        }
#endif /* DEBUG_MODE */
};

#endif /* CAUCHY_H */