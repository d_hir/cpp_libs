////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  CONTOUR.H
////  =========
////
////  HEADER FILE IMPLEMENTING A FIXED CONTOUR IN THE COMPLEX PLANE.
////  RIGHT NOW, THE CONTOUR IS FIXED TO CONTAIN EXACTLY 4 PARTS.
////
////  USAGE FOR CREATING A PARABOLIC CONTOUR WITH APEX m, UV-CUTOFF lambda,
////  np POINTS ON HALF-PARABOLA AND nv POINTS ON HALF-VERTICAL LINE:
////
////  Contour<O, C> c (np, nv, lambda, m);
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef CONTOUR_H
#define CONTOUR_H

/* FOR USING E.G. std::sinh */
#include <cmath>
/* STANDARD COMPLEX NUMBERS */
#include <complex>
/* STANDARD CONTAINER */
#include <vector>
/* FOR USING ASSERTIONS IN CASE OF INVALID ARGUMENTS */
#include <cassert>

/* FOR USING LEGENDRE POLYNOMIAL ROOTS */
#include "specialpolynomial.h"

#ifdef ALLOW_PLOT
/* TO PLOT CURVE */
#include "plotutils.h"
#endif /* ALLOW_PLOT */

/* TO BE ABLE TO USE 1.0i WITHOUT PREFIXES */
using namespace std::complex_literals;
/* THE FOLLOWING FUNCTIONS CAN BE USED WITHOUT 'STD::' PREFIX */
using std::abs;
using std::sqrt;
using std::sinh;
using std::cosh;
using std::asinh;

/* DEFINE A MACRO THAT SQUARES A NUMBER (PROBABLY FASTER THAN STD::POW) */
#ifndef SQR
#define SQR(x) ((x)*(x))
#endif /* SQR */

/* A CONSTANT THAT DESCRIBES HOW CLOSE A POINT CAN BE TO THE CONTOUR TO BE
   CONSIDERED 'INSIDE' THE CONTOUR */
#ifndef LITTLE_OVER_ONE
#define LITTLE_OVER_ONE 1.0
#endif /* LITTLE_OVER_ONE */

/* AN ENUMERATION FOR ALL PARTS OF THE CONTOUR */
enum class ContourPart {
    gamma_1 = 0, vertical_positive = 0,
    gamma_2 = 1, parabola_positive = 1,
    gamma_3 = 2, parabola_negative = 2,
    gamma_4 = 3, vertical_negative = 3
};

////////////////////////////////////////////////////////////////////////////////
// 
// CONTOURCREATOR CLASS
// ====================
//
// A INTERNAL CLASS THAT IS USED TO CREATE A SPECIFIC CONTOUR.
// IT HAS MEMBER FUNCTIONS THAT DESCRIBE THE CONTOUR PARTS AND ITS DERIVATIVES.
// 
////////////////////////////////////////////////////////////////////////////////
template<typename O = double, typename C = std::complex<O>>
class ContourCreator {
    private:
        /* THE UV CUTOFF OF THE CONTOUR */
        O lambda_sqr;
        /* THE MASS FOR THE QUARK DESCRIBED BY THE CONTOUR */
        O M;
        /* SQUARE OF M */
        O M_sqr;
        /* PRECALCULATED CONSTANTS FOR SPEEDUP */
        O c1;
        O c2;

    public:
        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::CONTOURCREATOR
        // ------------------------------
        // CONSTRUCTOR FOR CREATING A CONTOURCREATOR WITH GIVEN PARAMETERS
        //
        // IN:
        //   lambda_sqr_ (O) ... UV-CUTOFF OF CONTOUR
        //   M_          (O) ... MASS OF QUARK DESCRIBED BY CONTOUR
        // OUT:
        //   (CONTOURCREATOR)
        ////////////////////////////////////////////////////////////////////////
        ContourCreator(O lambda_sqr_, O M_) : 
                lambda_sqr(lambda_sqr_), M(M_), M_sqr(M_ * M_) {
            O root = sqrt(4.0 * lambda_sqr + M_sqr);
            c1 = M / 4.0 * root;
            c2 = 0.5 * asinh(root * 0.5);
        };

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z1
        // ------------------
        // METHOD THAT RETURNS A POINT ON THE FIRST PART OF THE CONTOUR
        // DEPENDING ON THE PARAMETER t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) POINT ON THE CONTOUR
        ////////////////////////////////////////////////////////////////////////
        C z1(O t) {
            return lambda_sqr + 1.0i * (1.0 + t) * c1;
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z2
        // ------------------
        // METHOD THAT RETURNS A POINT ON THE SECOND PART OF THE CONTOUR
        // DEPENDING ON THE PARAMETER t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) POINT ON THE CONTOUR
        ////////////////////////////////////////////////////////////////////////
        C z2(O t) {
            return SQR(sinh(c2 * (1.0 - t))) - M_sqr / 4.0 + 
                    1.0i * M * sinh(c2 * (1.0 - t));
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z3
        // ------------------
        // METHOD THAT RETURNS A POINT ON THE THIRD PART OF THE CONTOUR
        // DEPENDING ON THE PARAMETER t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) POINT ON THE CONTOUR
        ////////////////////////////////////////////////////////////////////////
        C z3(O t) {
            return SQR(sinh(c2 * (1.0 + t))) - M_sqr / 4.0 -
                    1.0i * M * sinh(c2 * (1.0 + t));
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z4
        // ------------------
        // METHOD THAT RETURNS A POINT ON THE FOURTH PART OF THE CONTOUR
        // DEPENDING ON THE PARAMETER t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) POINT ON THE CONTOUR
        ////////////////////////////////////////////////////////////////////////
        C z4(O t) {
            return lambda_sqr - 1.0i * (1.0 - t) * c1;
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z1P
        // -------------------
        // METHOD THAT RETURNS THE DERIVATIVE OF THE FIRST CONTOUR PART AT
        // PARAMETERIZATION POINT t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) DERIVATIVE OF CONTOUR IN GIVEN POINT
        ////////////////////////////////////////////////////////////////////////
        C z1p(O t) {
            return 1.0i * c1;
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z2P
        // -------------------
        // METHOD THAT RETURNS THE DERIVATIVE OF THE SECOND CONTOUR PART AT
        // PARAMETERIZATION POINT t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) DERIVATIVE OF CONTOUR IN GIVEN POINT
        ////////////////////////////////////////////////////////////////////////
        C z2p(O t) {
            return -c2 * sinh(2.0 * c2 * (1.0 - t)) -
                    1.0i * c2 * M * cosh(c2 * (1.0 - t));
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z3P
        // -------------------
        // METHOD THAT RETURNS THE DERIVATIVE OF THE THIRD CONTOUR PART AT
        // PARAMETERIZATION POINT t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) DERIVATIVE OF CONTOUR IN GIVEN POINT
        ////////////////////////////////////////////////////////////////////////
        C z3p(O t) {
            return c2 * sinh(2.0 * c2 * (1.0 + t)) -
                    1.0i * c2 * M * cosh(c2 * (1.0 + t));
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURCREATOR::Z4P
        // -------------------
        // METHOD THAT RETURNS THE DERIVATIVE OF THE FOURTH CONTOUR PART AT
        // PARAMETERIZATION POINT t
        //
        // IN:
        //   t (O) ... PARAMETERIZATION VARIABLE OF THE CONTOUR
        // OUT:
        //   (C) DERIVATIVE OF CONTOUR IN GIVEN POINT
        ////////////////////////////////////////////////////////////////////////
        C z4p(O t) {
            return 1.0i * c1;
        }
};

////////////////////////////////////////////////////////////////////////////////
// 
// CONTOURSEGMENT CLASS
// ====================
//
// A CLASS THAT REPRESENTS A SEGMENT / PART OF THE CONTOUR.
// 
////////////////////////////////////////////////////////////////////////////////
template<typename O = double, typename C = std::complex<O>>
class ContourSegment {
    private:
        /* REFERENCE TO A CONTOURCREATOR USED TO CREATE THIS CONTOURSEGMENT */
        ContourCreator<O, C>& contour_creator;
        /* NUMBER OF POINTS ON THIS SEGMENT */
        int num_points;
        /* ENUM ELEMENT KEEPING TRACK OF WHICH PART OF THE CONTOUR THIS IS */
        ContourPart part;

    public:
        /* VECTOR CONTAINING PARAMETERIZATION */
        std::vector<O> t;
        /* VECTOR CONTAINING POINTS ON THIS CONTOURSEGMENT */
        std::vector<C> z;
        /* VECTOR CONTAINING DERIVATIVE IN POINTS OF THIS CONTOURSEGMENT */
        std::vector<C> zp;

        ////////////////////////////////////////////////////////////////////////
        // CONTOURSEGMENT::CONTOURSEGMENT
        // ------------------------------
        // CONSTRUCTOR THAT CREATES A CONTOURSEGMENT USING A GIVEN 
        // CONTOURCREATOR WITH THE GIVEN NUMBER OF POINTS
        //
        // IN:
        //   contour_creator_ (CONTOURCREATOR<O, C>&) ... OBJECT FOR CREATION
        //   num_points       (INT)                   ... POINTS ON CONTOUR
        //   part_            (CONTOURPART)           ... WHICH PART OF CONTOUR
        // OUT:
        //   (CONTOURSEGMENT)
        ////////////////////////////////////////////////////////////////////////
        ContourSegment(ContourCreator<O, C>& contour_creator_, int num_points_,
                ContourPart part_) : 
                /* STORE ARGUMENTS AS MEMBERS */
                contour_creator(contour_creator_), 
                num_points(num_points_), 
                part(part_) {
            /* SET PARAMETERIZATION TO BE LEGENDRE ROOTS */
            t = double_polynomial.Legendre.getRoots(num_points);
            /* CALCULATE z AND z' FOR EACH t */
            for (auto& t_ : t) {
                switch (part) {
                    case ContourPart::gamma_1:
                        z.push_back(contour_creator.z1(t_));
                        zp.push_back(contour_creator.z1p(t_));
                        break;
                    case ContourPart::gamma_2:
                        z.push_back(contour_creator.z2(t_));
                        zp.push_back(contour_creator.z2p(t_));
                        break;
                    case ContourPart::gamma_3:
                        z.push_back(contour_creator.z3(t_));
                        zp.push_back(contour_creator.z3p(t_));
                        break;
                    case ContourPart::gamma_4:
                        z.push_back(contour_creator.z4(t_));
                        zp.push_back(contour_creator.z4p(t_));
                        break;
                    default:
                        std::cerr << "No valid contour part set!" << std::endl;
                        return;
                }
            }
        }
};

////////////////////////////////////////////////////////////////////////////////
// 
// CONTOUR CLASS
// =============
//
// A CLASS DESCRIBING A COMPLETE CONTOUR.
// THIS IS THE MAIN CLASS OF THIS MODULE.
// 
////////////////////////////////////////////////////////////////////////////////
template<typename O = double, typename C = std::complex<O>>
class Contour {
    private:
        /* A CONTOURCREATOR OBJECT USED TO CREATE THE CONTOURSEGMENTS */
        ContourCreator<O, C> contour_creator;
        /* THE MASS OF THE QUARK DESCRIBED BY THIS CONTOUR (APEX OF PARABOLA) */
        O M;
        /* THE UV-CUTOFF OF THE CONTOUR */
        O lambda_sqr;

    public:
        /* NUMBER OF POINTS ON A HALF-PARABOLA */
        int num_points_parabola;
        /* NUMBER OF POINTS ON A HALF-VERTICAL LINE */
        int num_points_vertical;
        /* VECTOR CONTAINING ALL SEGMENTS OF THIS CONTOUR */
        std::vector<ContourSegment<O, C> > segments;

        ////////////////////////////////////////////////////////////////////////
        // CONTOUR::CONTOUR
        // ----------------
        // CONSTRUCTOR THAT CREATES A CONTOUR FOR THE GIVEN ARGUMENTS AS
        // PARAMETERS
        //
        // IN:
        //   num_points_parabola_ (INT) ... NUMBER OF POINTS ON HALF-PARABOLA
        //   num_points_vertical_ (INT) ... NUMBER OF POINTS HALF-VERTICAL LINE
        //   lambda_sqr_          (O)   ... UV-CUTOFF OF CONTOUR
        //   M_                   (O)   ... MASS OF QUARK (APEX OF PARABOLA)
        // OUT:
        //   (CONTOUR)
        ////////////////////////////////////////////////////////////////////////
        Contour(int num_points_parabola_, int num_points_vertical_,
                O lambda_sqr_, O M_) : 
                /* CREATE CONTOURCREATOR FROM ARGUMENTS */
                contour_creator(lambda_sqr_, M_),
                /* STORE ARGUMENTS AS MEMBER VARIABLES */
                M(M_), lambda_sqr(lambda_sqr_),
                num_points_parabola(num_points_parabola_), 
                num_points_vertical(num_points_vertical_) {
            /* CREATE SEGMENTS AND STORE THEM IN segments VECTOR */
            for (int j = 0; j <= 3; j++) {
                int num_points = (j % 3 == 0) ? 
                        num_points_vertical : num_points_parabola;
                segments.emplace_back(contour_creator, num_points, 
                        ContourPart(j));
            }
        }
        
#ifdef ALLOW_PLOT
        ////////////////////////////////////////////////////////////////////////
        // CONTOUR::PLOT
        // -------------
        // METHOD THAT PLOTS THE CONTOUR IN THE COMPLEX PLANE.
        // THIS NEEDS THE 'plotutils.h' HEADER. IF IT IS AVAILABLE AND ONE WANTS
        // TO PLOT THE CONTOUR, SIMPLY DEFINE THE MACRO 'ALLOW_PLOT' BEFORE
        // INCLUDING THIS CONTOUR HEADER FILE.
        //
        // IN:
        //   -
        // OUT:
        //   -
        ////////////////////////////////////////////////////////////////////////
        void plot() const {
            /* CREATE FIGURE */
            plotutils::Figure<> f;
            /* CALCULATE TOTAL NUMBER OF POINTS OF CONTOUR */
            int num_points_total = 
                    2 * (num_points_vertical + num_points_parabola);
            /* DECLARE VECTOR CONTAINING THE REAL PART OF THE CONTOUR */
            std::vector<O> real;
            real.reserve(num_points_total);
            /* DECLARE VECTOR CONTAINING THE IMAGINARY PART OF THE CONTOUR */
            std::vector<O> imag;
            imag.reserve(num_points_total);
            /* RETRIEVE REAL AND IMAGINARY PART OF EVERY POINT ON THE CONTOUR */
            for (auto& seg : segments) {
                for (auto& z_ : seg.z) {
                    real.push_back(z_.real());
                    imag.push_back(z_.imag());
                }
            }
            /* PLOT IMAGINARY VS REAL PART */
            f.plot(real, imag);
        }
#endif

        ////////////////////////////////////////////////////////////////////////
        // CONTOUR::ISINSIDE
        // -----------------
        // A METHOD THAT CHECKS IF A GIVEN POINT LIES INSIDE OR OUTSIDE OF
        // THE CONTOUR.
        //
        // IN:
        //   z (C&) ... POINT TO CHECK IF INSIDE OR OUTSIDE OF CONTOUR
        // OUT:
        //   (BOOL) true IF z IS INSIDE CONTOUR, false OTHERWISE
        ////////////////////////////////////////////////////////////////////////
        bool isInside(const C& z) const {
            /* CHECK IF REAL PART IS BIGGER THAN LOWEST REAL POINT */
            bool re_big_enough = (- 4.0 * z.real() / M / M <= LITTLE_OVER_ONE);
            /* CHECK IF REAL PART IS LOWER THAN UV-CUTOFF */
            bool re_small_enough = (z.real() / lambda_sqr <= LITTLE_OVER_ONE);
            /* CHECK IF IMAGINARY PART IS LOW ENOUGH TO FIT INSIDE PARABOLA */
            bool abs_im_small_enough = ((abs(z.imag()) / 
                    sqrt(z.real() + M * M / 4.0) / M) <= LITTLE_OVER_ONE);
            /* ALL ABOVE CHECKS MUST BE TRUE TO BE INSIDE THE CONTOUR */
            return re_big_enough && re_small_enough && abs_im_small_enough;
        }
};

////////////////////////////////////////////////////////////////////////////////
// 
// CONTOURFUNCTION CLASS
// =====================
//
// A CLASS DESCRIBING A FUNCTION DEFINED ON THE CONTOUR
// 
////////////////////////////////////////////////////////////////////////////////
template<typename O = double, typename C = std::complex<O>>
class ContourFunction {
    protected:
        /* VALUES OF THE FUNCTION */
        /* EACH ENTRY OF THE OUTER VECTOR CORRESPONDS TO A SEGMENT ON CONTOUR */
        /* INNER VECTOR THEN IS THE FUNCTION ALONG THIS SEGMENT */
        std::vector<std::vector<C>> data;

    public:
        ////////////////////////////////////////////////////////////////////////
        // CONTOURFUNCTION::CONTOURFUNCTION
        // --------------------------------
        // CONTSTRUCTOR CREATING A CONTOURFUNCTION GIVEN A COMPLETE SET OF
        // FUNCTION VALUES.
        //
        // IN:
        //   data_ (VECTOR<VECTOR<C>>) ... FUNCTION VALUES ALONG CONTOUR
        // OUT:
        //   (CONTOURFUNCTION)
        ////////////////////////////////////////////////////////////////////////
        ContourFunction(std::vector<std::vector<C>> data_) : data(data_) {};

        ////////////////////////////////////////////////////////////////////////
        // CONTOURFUNCTION::CONTOURFUNCTION
        // --------------------------------
        // CONTSTRUCTOR CREATING A CONTOURFUNCTION GIVEN THE NUMBER OF POINTS
        // ON EACH PART OF THE CONTOUR AND AN INITIAL VALUE FOR THE FUNCTION.
        //
        // IN:
        //   num_vertical (INT) ... NUMBER OF POINTS ON VERTICAL LINE
        //   num_parabola (INT) ... NUMBER OF POINTS ON PARABOLA
        //   init         (C)   ... INITIAL VALUE FOR ALL FUNCTION POINTS
        // OUT:
        //   (CONTOURFUNCTION)
        ////////////////////////////////////////////////////////////////////////
        ContourFunction(int num_vertical, int num_parabola, C init) {
            // ContourPart::vertical_positive
            data.emplace_back(num_vertical, init);
            // ContourPart::parabola_positive
            data.emplace_back(num_parabola, init);
            // ContourPart::parabola_negative
            data.emplace_back(num_parabola, init);
            // ContourPart::vertical_negative
            data.emplace_back(num_vertical, init);
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURFUNCTION::OPERATOR[]
        // ---------------------------
        // ACCESS OPERATOR OVERLOAD TO GET REFERENCE TO ELEMENT IN data MEMBER
        //
        // IN:
        //   cp (INT) ... CONTOURPART ENTRY TO ACCESS SPECIFIC CONTOURSEGMENT
        // OUT:
        //   (VECTOR<C>&)
        ////////////////////////////////////////////////////////////////////////
        std::vector<C>& operator[] (const int cp) {
            assert((int) cp < 4 && (int) cp >= 0);
            return data[cp];
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURFUNCTION::OPERATOR[]
        // ---------------------------
        // ACCESS OPERATOR OVERLOAD TO GET VALUE IN data MEMBER
        //
        // IN:
        //   cp (INT) ... CONTOURPART ENTRY TO ACCESS SPECIFIC CONTOURSEGMENT
        // OUT:
        //   (VECTOR<C>)
        ////////////////////////////////////////////////////////////////////////
        std::vector<C> operator[] (const int cp) const {
            assert((int) cp < 4 && (int) cp >= 0);
            return data[cp];
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURFUNCTION::APPLYSCHWARTZREFLECTION
        // ----------------------------------------
        // USE SCHWARTZ REFLECTION PRINCIPLE TO DEDUCE VALUES OF FUNCTION VALUES
        // USING THEIR COMPLEX CONJUGATE VALUES
        //
        // IN:
        //   positive_to_negative_imag (BOOL) ... IF NEGATIVE IS DEDUCED FROM
        //                                        POSITIVE IMAGINARY PART
        // OUT:
        //   -
        ////////////////////////////////////////////////////////////////////////
        void applySchwartzReflection(bool positive_to_negative_imag = true) {
            if (positive_to_negative_imag) {
                /* COPY VALUES INVERSELY AND COMPLEX CONJUGATE (VERTICAL) */
                copyInverseConj(data[(int) ContourPart::vertical_negative], 
                        data[(int) ContourPart::vertical_positive]);
                /* COPY VALUES INVERSELY AND COMPLEX CONJUGATE (PARABOLA) */
                copyInverseConj(data[(int) ContourPart::parabola_negative],
                        data[(int) ContourPart::parabola_positive]);
            } else {
                /* COPY VALUES INVERSELY AND COMPLEX CONJUGATE (VERTICAL) */
                copyInverseConj(data[(int) ContourPart::vertical_positive],
                        data[(int) ContourPart::vertical_negative]);
                /* COPY VALUES INVERSELY AND COMPLEX CONJUGATE (PARABOLA) */
                copyInverseConj(data[(int) ContourPart::parabola_positive],
                        data[(int) ContourPart::parabola_negative]);
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURFUNCTION::SWAP
        // ---------------------
        // METHOD SWAPPING THE DATA OF THIS OBJECT WITH ANOTHER
        //
        // IN:
        //   other (CONTOURFUNCTION&) ... ANOTHER CONTOURFUNCTION OBJECT
        // OUT:
        //   -
        ////////////////////////////////////////////////////////////////////////
        void swap(ContourFunction<O, C>& other) {
            this->data.swap(other.data);
        }

        ////////////////////////////////////////////////////////////////////////
        // CONTOURFUNCTION::COPYINVERSECONJ
        // --------------------------------
        // METHOD COMPLEX CONJUGATING EVERY ELEMENT OF A VECTOR AND STORING
        // IT TO ANOTHER VECTOR IN INVERSE ORDER
        //
        // IN:
        //   to   (VECTOR<C>&) ... VECTOR TO STORE VALUES INTO
        //   from (VECTOR<C>&) ... VECTOR TO TAKE VALUES FROM
        // OUT:
        //   -
        ////////////////////////////////////////////////////////////////////////
        void copyInverseConj(std::vector<C>& to, const std::vector<C>& from) {
            for (int j = 0; j < (int) to.size(); j++)
                to[j] = std::conj(from[from.size() - j - 1]);
        }
};

#endif /* CONTOUR_H */