////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  NEWTON.H
////  ========
////
////  HEADER FILE FOR NEWTON ROOT FINDING ALGORITHM
////  USAGE FOR FINDING ROOT OF FUNCTION f WITH STARTING GUESS x_0:
////
////  result = Newton<T>::findRoot(&f, x_0);
////
////  IN THIS CASE, NUMERICAL DIFFERENTIATION WILL BE USED TO DETERMINE THE
////  DERIVATIVE OF f. THE CORRESPONDING "INFINITESIMAL" STEPSIZE h IS STORED
////  INSIDE Newton<T>::DIFFERENTIATION_H.
////
////  ALTERNATIVELY, IF ONE KNOWS THE ANALYTICAL DERIVATIVE OF f, IT CAN ALSO
////  BE PASSED TO THE ALGORITHM TO SPEED UP THE CALCULATION AND MAKE IT
////  MORE PRECISE. FOR A DERIVATIVE fp, THIS MODULE IS USED AS FOLLOWS:
////
////  result = Newton<T>::findRoot(&f, &fp, x_0);
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef NEWTON_H
#define NEWTON_H

////////////////////////////////////////////////////////////////////////////////
// 
// NEWTON CLASS
// ============
//
// A STATIC CLASS USED FOR FINDING A ROOT OF A FUNCTION GIVEN ANALYTICALLY.
// WOULD BE BETTER SUITED AS A NAMESPACE THAN A CLASS. MAY BE REWRITTEN IN 
// THE FUTURE.
//
////////////////////////////////////////////////////////////////////////////////
template<typename T = double>
class Newton {
  public:
    /* DISTANCE BETWEEN ADJACENT ROOT GUESSES TO BE CONSIDERED CONVERGED */
    static T CONVERGED_DIFFERENCE;
    /* x SPACING USED FOR THE NUMERICAL DIFFERENTIATION */
    static T DIFFERENTIATION_H;
    /* MAXIMUM NUMBER OF ROOT GUESSES BEFORE ABORTING SEARCH */
    static int MAX_ITERATIONS;

    ////////////////////////////////////////////////////////////////////////////
    // NEWTON::FINDROOT
    // ----------------
    // METHOD USED FOR FINDING THE ROOT OF A FUNCTION USING NUMERICAL 
    // DIFFERENTIATION
    //
    // TEMPLATE:
    //   fct_t FUNCTION POINTER OR FUNCTOR TYPE
    // IN:
    //   f (fct_t) ... FUNCTION WHERE ROOT IS SEARCHED FOR
    //   x (T)     ... STARTING GUESS FOR NEWTON ALGORITHM
    // OUT:
    //   (T) A ROOT OF f FOUND BY NEWTON ALGORITHM
    ////////////////////////////////////////////////////////////////////////////
    template<typename fct_t>
    static T findRoot(fct_t f, T x) {
      /* STORAGE FOR PREVIOUS GUESS */
      T x_prev;
      /* VARIABLE COUNTING ITERATIONS OF ALGORITHM */
      int current_iteration = 0;
      /* LOOP RUNNING TILL CONVERGED OR MAX_ITERATIONS REACHED */
      do {
        /* INCREMENT NUMBER OF CURRENT ITERATIONS */
        current_iteration++;
        /* STORING PREVIOUS GUESS */
        x_prev = x;
        /* CALCULATING NEW GUESS USING NEWTON ALGORITHM */
        /* NUMERICAL DIFFERENTIATION IS USED TO GET f'  */
        x = x_prev - DIFFERENTIATION_H / 
            (f(x_prev + DIFFERENTIATION_H) / f(x_prev) - 1);
      } while (std::abs(x_prev - x) > CONVERGED_DIFFERENCE 
          && current_iteration < MAX_ITERATIONS);
      /* RETURN LAST GUESS */
      /* MAY BE GARBAGE IF MAX_ITERATIONS HAS BEEN REACHED */
      return x;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // NEWTON::FINDROOT
    // ----------------
    // METHOD USED FOR FINDING THE ROOT OF A FUNCTION USING AN ANALYTICAL 
    // DERIVATIVE
    //
    // TEMPLATE:
    //   fct_t FUNCTION POINTER OR FUNCTOR TYPE
    // IN:
    //   f (fct_t)            ... FUNCTION WHERE ROOT IS SEARCHED FOR
    //   f_derivative (fct_t) ... DERIVATIVE OF f AS fct_t TYPE
    //   x (T)                ... STARTING GUESS FOR NEWTON ALGORITHM
    // OUT:
    //   (T) A ROOT OF f FOUND BY NEWTON ALGORITHM
    ////////////////////////////////////////////////////////////////////////////
    template<typename fct_t>
    static T findRoot(fct_t f, fct_t f_derivative, T x) {
      /* STORAGE FOR PREVIOUS GUESS */
      T x_prev;
      /* VARIABLE COUNTING ITERATIONS OF ALGORITHM */
      int current_iteration = 0;
      /* LOOP RUNNING TILL CONVERGED OR MAX_ITERATIONS REACHED */
      do {
        /* INCREMENT NUMBER OF CURRENT ITERATIONS */
        current_iteration++;
        /* STORING PREVIOUS GUESS */
        x_prev = x;
        /* CALCULATING NEW GUESS USING NEWTON ALGORITHM */
        /* THE ANALYTICAL DERIVATIVE IS USED FOR f'     */
        x = x_prev - f(x_prev) / f_derivative(x_prev);
      } while (std::abs(x_prev - x) > CONVERGED_DIFFERENCE 
          && current_iteration < MAX_ITERATIONS);
      /* RETURN LAST GUESS */
      /* MAY BE GARBAGE IF MAX_ITERATIONS HAS BEEN REACHED */
      return x;
    }
};

/* DISTANCE BETWEEN ADJACENT ROOT GUESSES TO BE CONSIDERED CONVERGED */
template<typename T>
T Newton<T>::CONVERGED_DIFFERENCE = T(1e-8);

/* x SPACING USED FOR THE NUMERICAL DIFFERENTIATION */
template<typename T>
T Newton<T>::DIFFERENTIATION_H = T(1e-6);

/* MAXIMUM NUMBER OF ROOT GUESSES BEFORE ABORTING SEARCH */
template<typename T>
int Newton<T>::MAX_ITERATIONS = 1000;

#endif /* NEWTON_H */