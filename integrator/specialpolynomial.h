////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  SPECIALPOLYNOMIAL.H
////  ===================
////
////  HEADER FILE FOR USING LEGENDRE, CHEBYCHEV OR SIMILAR POLYNOMIALS
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef SPECIALPOLYNOMIAL_H
#define SPECIALPOLYNOMIAL_H

/* STANDARD CONTAINER USED WITH THIS MODULE */
#include <vector>
/* MATHEMATICAL OPERATIONS LIKE cos */
#include <cmath>
/* FOR DEBUG MESSAGES */
#include <iostream>

/* TO FIND ROOTS OF POLYNOMIALS */
#include "newton.h"

/* IF THE LEGENDRE POLYNOMIALS SHOULD BE EVALUATED USING PURE RECURSION */
/* 0 ... USE LOOP TO ITERATE THROUGH DIFFERENT ORDERS (FASTER!)         */
/* 1 ... USE PURE RECURSION TO GET LOWER ORDERS (MORE INTUITIVE!)       */
#define USE_PURE_RECURSION 0

/* DEFINE MATHEMATICAL CONSTANT PI */
#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950  
#endif /* M_PI */

/* FORWARD DECLARE SPECIALPOLYNOMIAL CLASS */
template<typename T>
class SpecialPolynomial;

////////////////////////////////////////////////////////////////////////////////
// 
// POLYNOMIALFUNCTOR CLASS
// =======================
//
// FUNCTOR THAT ALLOWS PASSING A SPECIAL POLYNOMIAL AS IF IT WERE A FUNCTION
// POINTER. THIS STEP IS NEEDED AS THE POLYNOMIALS ARE TEMPLATED CLASSES
// AND ITS METHOD TO GET THE VALUE OF THE POLYNOMIAL CANNOT BE PASSED AROUND
// USING STANDARD FUNCTION POINTERS.
//
// ONCE CREATED, CAN BE USED LIKE A FUNCTION AND ALSO PASSED AS ARGUMENT.
//
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class PolynomialFunctor {
    private:
        /* ORDER OF POLYNOMIAL */
        int n;
        /* POINTER TO THE ACTUAL POLYNOMIAL */
        SpecialPolynomial<T>* p;
        /* IF THE DERIVATIVE OF THE POLYNOMIAL IS WANTED */
        bool get_derivative;
    
    public:
        ////////////////////////////////////////////////////////////////////////
        // POLYNOMIALFUNCTOR::POLYNOMIALFUNCTOR
        // ------------------------------------
        // CONSTRUCTOR FOR CREATING A FUNCTOR TO A GIVEN SPECIAL POLYNOMIAL
        //
        // IN:
        //   n_ (int) 					... ORDER OF THE POLYNOMIAL
        //   p_ (SpecialPolynomial<T>*) ... POINTER TO THE POLYNOMIAL
        //   get_derivative_ (bool)     ... WHETHER DERIVATIVE OF p_ IS USED
        // OUT:
        //   (PolynomialFunctor) FUNCTOR REDIRECTING CALL TO POLYNOMIAL
        ////////////////////////////////////////////////////////////////////////
        PolynomialFunctor(int n_, SpecialPolynomial<T>* p_, 
                bool get_derivative_) : n(n_), p(p_), 
                get_derivative(get_derivative_)	{}

        ////////////////////////////////////////////////////////////////////////
        // POLYNOMIALFUNCTOR::OPERATOR()
        // -----------------------------
        // OVERLOAD TO THE ()-OPERATOR. REDIRECTS TO CALLING SPECIALPOLYNOMIAL.
        //
        // IN:
        //   x (T) ... POINT WHERE SPECIALPOLYNOMIAL SHALL BE EVALUATED
        // OUT:
        //   (T) RETURN VALUE OF SPECIALPOLYNOMIAL CALL
        ////////////////////////////////////////////////////////////////////////
        T operator() (T x) const {
            /* RETURN EITHER DERIVATIVE OR POLYNOMIAL ITSELF */
            if (this->get_derivative)
                return p->derivative(this->n, x);
            else
                return p->evaluate(this->n, x);
        }
};

////////////////////////////////////////////////////////////////////////////////
// 
// SPECIALPOLYNOMIAL CLASS
// =======================
//
// ABSTRACT SPECIALPOLYNOMIAL CLASS DESCRIBING A STANDARD POLYNOMIAL.
// DIFFERENT POLYNOMIAL TYPES SHALL INHERIT FROM THIS SUPERCLASS.
//
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class SpecialPolynomial {
    public:
        /* WHETHER AN ANALYTICAL DERIVATIVE SHALL BE USED FOR ROOT FINDING */
        static bool ANALYTICAL_DERIVATIVE;
        
        ////////////////////////////////////////////////////////////////////////
        // SPECIALPOLYNOMIAL::EVALUATE
        // ---------------------------
        // METHOD WHICH RETURNS THE VALUE OF THE POLYNOMIAL OF ORDER n AT
        // POINT x.
        // NEEDS TO BE REPLACED BY DERIVED CLASSES.
        //
        // IN:
        //   n (int) ... ORDER OF THE POLYNOMIAL IN QUESTION
        //   x (T)   ... POINT WHERE SPECIALPOLYNOMIAL SHALL BE EVALUATED
        // OUT:
        //   (T) VALUE OF THE SPECIFIED POLYNOMIAL AT POINT x
        ////////////////////////////////////////////////////////////////////////
        virtual T evaluate(const int n, const T x) = 0;

        ////////////////////////////////////////////////////////////////////////
        // SPECIALPOLYNOMIAL::DERIVATIVE
        // -----------------------------
        // METHOD WHICH RETURNS THE DERIVATIVE OF THE POLYNOMIAL OF ORDER n AT
        // POINT x.
        //
        // IN:
        //   n (int) ... ORDER OF THE POLYNOMIAL IN QUESTION
        //   x (T)   ... POINT WHERE DERIVATIVE SHALL BE EVALUATED
        // OUT:
        //   (T) VALUE OF THE DERIVATIVE OF THE SPECIFIED POLYNOMIAL AT POINT x
        ////////////////////////////////////////////////////////////////////////
        virtual T derivative(const int n, const T x);

        ////////////////////////////////////////////////////////////////////////
        // SPECIALPOLYNOMIAL::ROOT
        // -----------------------
        // METHOD WHICH RETURNS A ROOT OF THE POLYNOMIAL OF ORDER n WITH
        // STARTING GUESS x_0 USING NEWTON ALGORITHM.
        //
        // IN:
        //   n   				  (int) ... ORDER OF THE POLYNOMIAL
        //   x_0 				  (T)   ... STARTING GUESS FOR NEWTON ALGORITHM
        //   converged_difference (T)   ... ACCURACY USED FOR NEWTON ALGORITHM
        // OUT:
        //   (T) ROOT OF THE POLYNOMIAL
        ////////////////////////////////////////////////////////////////////////
        T root(const int n, const T x_0, const T converged_difference = T(0)) {
            /* IF CONVERGED_DIFFERENCE IS PASSED, IT GETS SET */
            if (converged_difference != T(0))
                Newton<T>::CONVERGED_DIFFERENCE = converged_difference;
            /* POLYNOMIAL GETS WRAPPED IN FUNCTOR AND PASSED TO ROOT FINDING */
            if (SpecialPolynomial::ANALYTICAL_DERIVATIVE)
                return Newton<T>::findRoot(PolynomialFunctor<T>(n, this, false),
                        PolynomialFunctor<T>(n, this, true), x_0);
            else
                return Newton<T>::findRoot(
                        PolynomialFunctor<T>(n, this, false), x_0);
        };

        ////////////////////////////////////////////////////////////////////////
        // SPECIALPOLYNOMIAL::WEIGHTS
        // --------------------------
        // METHOD WHICH RETURNS THE WEIGHTS USED IN GAUSS INTEGRATION.
        //
        // IN:
        //   x (std::vector<T>&)   ... VECTOR WITH ROOTS OF POLYNOMIAL
        //   n (int) 			   ... ORDER OF THE GAUSS INTEGRATION
        // OUT:
        //   (std::vector<T>) VECTOR CONTAINING WEIGHTS
        ////////////////////////////////////////////////////////////////////////
        virtual std::vector<T> weights(std::vector<T>& x, int n);

        ////////////////////////////////////////////////////////////////////////
        // SPECIALPOLYNOMIAL::GETROOTS
        // ---------------------------
        // METHOD WHICH RETURNS ALL ROOTS OF THE POLYNOMIAL
        //
        // IN:
        //   n 					  (int) ... ORDER OF THE POLYNOMIAL
        //   converged_difference (T)   ... ACCURACY OF NEWTON ALGORITHM
        // OUT:
        //   (std::vector<T>) VECTOR CONTAINING ROOTS
        ////////////////////////////////////////////////////////////////////////
        virtual std::vector<T> getRoots(int n, T converged_difference = T(0));

        ////////////////////////////////////////////////////////////////////////
        // SPECIALPOLYNOMIAL::GETGAUSSINTEGRATIONG
        // ---------------------------------------
        // IF POLYNOMIAL USED WITH GAUSS QUADRATURE, THE FUNCTION THAT IS 
        // EXCLUDED FROM THE INTEGRAND (OFTEN CALLED g(z)) CAN BE GOTTEN
        // THROUGH THIS METHOD.
        // E.G. FOR 
        //		- LEGENDRE: 		  g(z) = 1
        //		- CHEBYCHEV 1ST KIND: g(z) = 1 / sqrt(1 - z^2)
        //		- CHEBYCHEV 2ND KIND: g(z) = sqrt(1 - z^2)
        //
        // IN:
        //   z (T) ... POINT WHERE g(z) IS WANTED
        // OUT:
        //   (T) VALUE OF g(z)
        ////////////////////////////////////////////////////////////////////////
        virtual T getGaussIntegrationG(T z);
};

/* WHETHER AN ANALYTICAL DERIVATIVE SHALL BE USED FOR ROOT FINDING */
template<typename T>
bool SpecialPolynomial<T>::ANALYTICAL_DERIVATIVE = false;

////////////////////////////////////////////////////////////////////////////////
// 
// LEGENDREPOLYNOMIAL CLASS : SPECIALPOLYNOMIAL
// ============================================
//
// CONCRETE POLYNOMIAL CLASS DESCRIBING LEGENDRE POLYNOMIALS.
//
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class LegendrePolynomial : public SpecialPolynomial<T> {
    public:
        /* A CALL TO EVALUATE ALWAYS STORES ALSO THE SECOND HIGHEST ORDER */
        /* USED TO PREVENT CALLING EVALUATE TWICE FOR DERIVATIVE */
        T prev = 0.0;

        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::EVALUATE */
        T evaluate(const int n, const T x) {
            /* TRIVIAL IF ORDER 0 OR 1 */
            if (n == 0)
                return 1;
            else if (n == 1)
                return x;
            /* IN CASE OF PURE RECURSION SIMPLY CALL ITSELF TWICE */
#if USE_PURE_RECURSION
            else
                return (2.0 * n - 1.0) / n * x * evaluate(n - 1, x) -
                        (n - 1.0) / n * evaluate(n - 2, x);
#else /* USE_PURE_RECURSION */
            /* OTHERWISE STORE RESULTS IN VECTOR */
            /* SIZE n + 1 NEEDED AS WE ALSO NEED ORDER 0 */
            std::vector<T> lp (n + 1);
            /* SET ORDER 0 AND 1 */
            lp[0] = 1;
            lp[1] = x;
            /* FILL HIGHER ORDERS USING THE LOWER ONES */
            for (int n_ = 2; n_ <= n; n_++)
                lp[n_] = (2.0 * n_ - 1.0) / n_ * x * lp[n_ - 1] -
                        (n_ - 1.0) / n_ * lp[n_ - 2];
            /* STORE SECOND HIGHEST ORDER VALUE */
            prev = *(lp.end() - 2);
            /* RETURN HIGHEST ORDER VALUE */
            return lp.back();
#endif /* USE_PURE_RECURSION */
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::DERIVATIVE */
        T derivative(const int n, const T x) {
            /* IN CASE OF ORDER 0 TRIVIAL */
            if (n == 0)
                return 1;
            else {
                /* OTHERWISE CALCULATE IT USING EVALUATE */
                T evaluation = evaluate(n, x);
                return (prev - x * evaluation) * n / (1 - x*x);
            }
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::WEIGHTS */
        std::vector<T> weights(std::vector<T>& xi, int order) {
            /* CREATE SPACE FOR WEIGHTS */
            std::vector<T> weights (order);
            /* LOOP THROUGH ALL WEIGHTS */
            for (int j = 0; j < order; j++) {
                /* GET DERIVATIVE OF THE POLYNOMIAL */
                T p_n_prime = derivative(order, xi[j]);
                /* CALCULATE WEIGHTS */
                weights[j] = 2 / (1 - xi[j]*xi[j]) / (p_n_prime * p_n_prime);
            }
            /* RETURN WEIGHTS */
            return weights;
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::GETROOTS */
        std::vector<T> getRoots(int order, T converged_difference = T(0)) {
            /* CREATE SPACE FOR ROOTS */
            std::vector<T> roots (order);
            /* ITERATE THROUGH ALL ROOTS */
            for (int i = 0; i < order; i++)
            /* CALL ROOT FINDING ALGORITHM USING SPECIFIC STARTING GUESSES */
                roots[order - i - 1] = this->root(order, 
                        T(cos(M_PI * (i + 1 - 0.25) / (order + 0.5))), 
                        converged_difference);
            /* RETURN FOUND ROOTS */
            return roots;
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL */
        /* SPECIALPOLYNOMIAL::GETGAUSSINTEGRATIONG */
        T getGaussIntegrationG(T z = T(0)) {
            /* g(z) = 1 FOR GAUSS LEGENDRE QUADRATURE */
            return 1.0;
        };
};

////////////////////////////////////////////////////////////////////////////////
// 
// CHEBYCHEVPOLYNOMIALFIRSTKIND CLASS : SPECIALPOLYNOMIAL
// ======================================================
//
// CONCRETE POLYNOMIAL CLASS DESCRIBING CHEBYCHEV POLYNOMIALS OF FIRST KIND.
//
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class ChebychevPolynomialFirstKind : public SpecialPolynomial<T> {
    public:
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::EVALUATE */
        T evaluate(const int n, const T x) {
            /* TRIVIAL FOR ORDER 0 AND 1 */
            if (n == 0)
                return 1;
            else if (n == 1)
                return x;
            /* OTHERWISE CALCULATE USING PURE RECURSION */
            else
                return 2 * x * evaluate(n - 1, x) - evaluate(n - 2, x);
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::DERIVATIVE */
        T derivative(const int n, const T x) {
            /* THIS METHOD HAS NOT YET BEEN IMPLEMENTED! */
            std::cout << "CHEBYCHEV FIRST KIND DERIVATIVE NOT IMPLEMENTED" 
                    << std::endl;
            return T(0);
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::GETROOTS */
        std::vector<T> getRoots(int order, T UNUSED = T(0)) {
            /* CREATE SPACE FOR ROOTS */
            std::vector<T> roots (order);
            /* CALCULATE ALL ROOTS USING SPECIFIC STARTING GUESSES */
            for (int k = 0; k < order; k++)
                roots[order - k - 1] = T(cos(M_PI * (k + 0.5) / order));
            /* RETURN ALL FOUND ROOTS */
            return roots;
        };
        
        ////////////////////////////////////////////////////////////////////////
        // CHEBYCHEVPOLYNOMIALFIRSTKIND::D_INTERPOLATION
        // ---------------------------------------------
        // METHOD WHICH RETURNS THE WEIGHTS d_i USED IN CHEBYCHEV INTERPOLATION.
        //
        // IN:
        //   f_at_roots (std::vector<T>&)   ... VECTOR OF f(ROOTS OF POLYNOMIAL)
        //   m (int) 			   			... ORDER OF THE INTERPOLATION
        // OUT:
        //   (std::vector<T>) VECTOR CONTAINING WEIGHTS
        ////////////////////////////////////////////////////////////////////////
        std::vector<T> d_interpolation(std::vector<T>& f_at_roots, int M) {
            /* GET ORDER N */
            int N = f_at_roots.size();
            /* RESERVE SPACE FOR WEIGHTS */
            std::vector<T> weights (M, 0);
            /* CALCULATE WEIGHTS USING LOOP TO EVALUATE SUM */
            for (int i = 0; i < M; i++) {
                for (int k = 0; k < N; k++)
                    weights[i] += f_at_roots[N - k - 1] * 
                            cos(M_PI * i * (k + 0.5) / N);
                weights[i] *= 2.0 / N;
            }
            /* RETURN WEIGHTS */
            return weights;
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::WEIGHTS */
        std::vector<T> weights(std::vector<T>& UNUSED, int N) {
            /* EVERY WEIGHTS IS PI/N */
            return std::vector<T> (N, M_PI / N);
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL */
        /* SPECIALPOLYNOMIAL::GETGAUSSINTEGRATIONG */
        T getGaussIntegrationG(T z) {
            return 1.0 / sqrt(1.0 - z * z);
        };
};

////////////////////////////////////////////////////////////////////////////////
// 
// CHEBYCHEVPOLYNOMIALSECONDKIND CLASS : SPECIALPOLYNOMIAL
// =======================================================
//
// CONCRETE POLYNOMIAL CLASS DESCRIBING CHEBYCHEV POLYNOMIALS OF SECOND KIND.
//
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class ChebychevPolynomialSecondKind : public SpecialPolynomial<T> {
    public:
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::EVALUATE */
        T evaluate(const int n, const T x) {
            /* ORDER 0 AND 1 ARE TRIVIAL; OTHERWISE USE PURE RECURSION */
            if (n == 0)
                return 1;
            else if (n == 1)
                return 2 * x;
            else
                return 2 * x * evaluate(n - 1, x) - evaluate(n - 2, x);
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::DERIVATIVE */
        T derivative(const int n, const T x) {
            /* THIS METHOD HAS NOT YET BEEN IMPLEMENTED! */
            std::cout << "CHEBYCHEV SECOND KIND DERIVATIVE NOT IMPLEMENTED" 
                    << std::endl;
            return T(0);
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::GETROOTS */
        std::vector<T> getRoots(int order, T UNUSED = T(0)) {
            /* RESERVE SPACE FOR ROOTS */
            std::vector<T> roots (order);
            /* ROOTS HAVE ANALYTICAL FORMULA */
            for (int k = 0; k < order; k++)
                roots[order - k - 1] = T(cos(M_PI * (k + 1) / (order + 1)));
            /* RETURN ROOTS */
            return roots;
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL SPECIALPOLYNOMIAL::WEIGHTS */
        std::vector<T> weights(std::vector<T>& UNUSED, int N) {
            /* RESERVE SPACE FOR WEIGHTS */
            std::vector<T> weights (N);
            /* CALCULATE ALL WEIGHTS DIRECTLY */
            for (int i = 1; i <= N; i++)
                weights[i - 1] = M_PI / (N + 1) * 
                        pow(sin(double(i) / (N + 1) * M_PI), 2);
            /* RETURN WEIGHTS */
            return weights;
        };
        
        /* FOR A BETTER DESCRIPTION SEE VIRTUAL */
        /* SPECIALPOLYNOMIAL::GETGAUSSINTEGRATIONG */
        T getGaussIntegrationG(T z) {
            return sqrt(1.0 - z * z);
        };
};

////////////////////////////////////////////////////////////////////////////////
// 
// POLYNOMIALSTRUCT STRUCT [double_polynomial]
// ===========================================
//
// STRUCT COLLECTING ALL AVAILABLE POLYNOMIALS AS TYPE DOUBLE.
// THIS IS USEFUL IF ONE WANTS A CONCRETE POLYNOMIAL WITHOUT NEEDING TO 
// CREATE ONE BEFOREHAND.
//
////////////////////////////////////////////////////////////////////////////////
struct PolynomialStruct {
    LegendrePolynomial<double> Legendre;
    ChebychevPolynomialFirstKind<double> ChebychevFirstKind;
    ChebychevPolynomialSecondKind<double> ChebychevSecondKind;
} double_polynomial;

#endif /* SPECIALPOLYNOMIAL_H */
