#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <iostream>

class Timer {
  private:
    std::chrono::high_resolution_clock::time_point startpoint;
    std::chrono::high_resolution_clock::time_point endpoint;
    double timedelta = 0;
    
  public:
    Timer(){};
    Timer(double timedelta_);
    void start();
    void end();
    double get() const;
    
    friend std::ostream& operator<<(std::ostream& stream, const Timer& timer);
};

Timer::Timer (double timedelta_) {
  this->timedelta = timedelta_;
};

void Timer::start() {
  this->startpoint = std::chrono::high_resolution_clock::now();
};

void Timer::end() {
  this->endpoint = std::chrono::high_resolution_clock::now();
};

double Timer::get() const {
  return std::chrono::duration_cast<std::chrono::nanoseconds>
      (this->endpoint - this->startpoint).count();
};

std::ostream& operator<<(std::ostream& stream, const Timer& timer) {
  double duration = timer.timedelta;
  if (timer.timedelta == 0)
    duration = timer.get();
  if (duration < 1e3)
    stream << duration << " ns";
  else if (duration < 1e6)
    stream << duration / 1e3 << " us";
  else if (duration < 1e9)
    stream << duration / 1e6 << " ms";
  else
    stream << duration / 1e9 << " s";
  return stream;
};

#endif
