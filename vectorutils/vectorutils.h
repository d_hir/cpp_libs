////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  VECTORUTILS.H
////  =============
////
////  HEADER FILE CONTAINING SEVERAL USEFUL TOOLS FOR HANDLING STD::VECTORS
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef VECTORUTILS_H
#define VECTORUTILS_H

// OBVIOUSLY USING STD::VECTORS
#include <vector>
// FOR USING E.G. STD::PLUS
#include <cmath>
// FOR LU FACTORIZATION FOR POLYFIT
#include <boost/numeric/ublas/lu.hpp>
// FOR POLYFIT
#include <boost/numeric/ublas/matrix.hpp>
// THROW ERRORS ON INVALID ARGUMENTS / INVALID VALUES
#include <assert.h>
// FOR USING EXCEPTIONS FROM STANDARD LIBRARY
#include <stdexcept>
// FOR FILE-IO
#include <fstream>
// FOR USING STRINGS
#include <string>
// FOR USING STD::ACCUMULATE FOR INSTANCE
#include <numeric>
// FOR CONVERTING TO COMPLEX NUMBERS AND BACK
#include <complex>

// WRAP EVERYTHING INTO ITS OWN NAMESPACE
namespace vectorutils {

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::PRINTCHAR
// ----------------------
// PRINTS A GIVEN CHARACTER A GIVEN NUMBER OF TIMES TO STDOUT
//
// IN:
//   c       (CHAR) ... CHARACTER TO PRINT TO STDOUT
//   amount  (INT)  ... NUMBER OF c TO BE PRINTED (DEFAULT: 80)
//   newline (BOOL) ... WHETHER A NEWLINE CHARACTER SHALL BE PRINTED AFTERWARDS
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
void printChar(char c, int amount = 80, bool newline = true) {
  for (int j = 0; j < amount; j++)
    std::cout << c;
  if (newline)
    std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::MAKEREAL
// ---------------------
// FUNCTION WHICH CREATES A REAL VECTOR FROM A COMPLEX VECTOR
//
// IN:
//   cvec (VECTOR<COMPLEX<T>>&) ... VECTOR NEEDED TO BE MADE REAL
// OUT:
//   (VECTOR<T>) SAME VECTOR AS cvec BUT AS T TYPE
////////////////////////////////////////////////////////////////////////////////
template<typename T>
std::vector<T> makeReal(const std::vector<std::complex<T>>& cvec) {
  std::vector<T> rvec;
  rvec.reserve(cvec.size());
  for (const std::complex<T>& c : cvec)
    rvec.emplace_back(c.real());
  return rvec;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::MAKECOMPLEX
// ------------------------
// FUNCTION WHICH CREATES A COMPLEX VECTOR FROM A REAL VECTOR
//
// IN:
//   rvec (VECTOR<T>&) ... VECTOR NEEDED TO BE MADE COMPLEX
// OUT:
//   (VECTOR<COMPLEX<T>>) SAME VECTOR AS rvec BUT AS STD::COMPLEX TYPE
////////////////////////////////////////////////////////////////////////////////
template<typename T>
std::vector<std::complex<T>> makeComplex(const std::vector<T>& rvec) {
  std::vector<std::complex<T>> cvec;
  cvec.reserve(rvec.size());
  for (const T& r : rvec)
    cvec.emplace_back(r);
  return cvec;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::PRINT
// ------------------
// FUNCTION WHICH PRINTS A GIVEN VECTOR TO STDOUT
//
// IN:
//   x (VECTOR<T>&) ... VECTOR TO PRINT TO STDOUT
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
template<typename T>
void print(const std::vector<T> &x, std::string title_str = "", bool column_wise = false) {
  if (title_str != "")
    title_str = " [ " + title_str + " ] ";
  int num_insertions_per_side = (80 - title_str.length()) / 2;
  for (int j = 0; j < num_insertions_per_side; j++) {
    title_str.insert(0, "-");
    title_str += "-";
  }
  std::cout << "-------" << title_str << "-------" << std::endl;
  for (auto& e : x) {
    std::cout << e;
    if (column_wise)
      std::cout << std::endl;
    else if (e != x.back())
      std::cout << ", ";
  }
  if (!column_wise)
    std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::PRINT
// ------------------
// FUNCTION WHICH PRINTS A CONTAINER GIVEN AN BEGINNING AND ENDING ITERATOR TO
// STDOUT
//
// IN:
//   begin_it (ITERATOR/it) ... ITERATOR TO BEGINNING OF PRINT
//   end_it   (ITERATOR/it) ... ITERATOR TO END OF PRINT
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
template<typename It>
void print(It begin_it, It end_it) {
  std::cout << "--------------" << std::endl;
  for (It loop_it = begin_it; loop_it <= end_it; loop_it++)
    std::cout << *loop_it << std::endl;
}


////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::PRINT
// ------------------
// FUNCTION WHICH PRINTS 2 VECTORS SIDE BY SIDE TO STDOUT
//
// IN:
//   x1 (VECTOR<T>&) ... FIRST VECTOR TO PRINT TO STDOUT
//   x2 (VECTOR<T>&) ... SECOND VECTOR TO PRING TO STDOUT
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
template<typename T, typename U>
void print(std::vector<T> &x1, std::vector<U> &x2) {
  if (x1.size() != x2.size()) {
    std::cout << " VECTORS NOT MATCHING IN SIZE!\n" <<
        " PRINTING ABORTED!" << std::endl;
    return;
  }
  std::cout << "--------------" << std::endl;
  for (int j = 0; j < static_cast<int>(x1.size()); j++)
    std::cout << x1[j] << "   |   " << x2[j] << std::endl;
}


////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::MEAN
// -----------------
// CALCULATES THE MEAN OF A VECTOR
//
// IN:
//   x (VECTOR<T>&) ... VECTOR WHOSE MEAN IS TO BE CALCULATED
// OUT:
//   (T) MEAN OF x
////////////////////////////////////////////////////////////////////////////////
template<typename T>
T mean(const std::vector<T> &x) {
  return std::accumulate(x.begin(), x.end(), 0.0) / x.size();
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::MEAN
// -----------------
// CALCULATES THE MEAN OF A CONTAINER BETWEEN begin_it AND end_it ITERATORS
//
// IN:
//   begin_it (ITERATOR / it) ... ITERATOR TO BEGINNING
//   end_it   (ITERATOR / it) ... ITERATOR TO END
// OUT:
//   (TYPE WHERE it POINTS TO) MEAN OF VALUES BETWEEN begin_it AND end_it
////////////////////////////////////////////////////////////////////////////////
template<typename Iter>
typename std::iterator_traits<Iter>::value_type mean(const Iter begin_it,
    const Iter end_it) {
  return std::accumulate(begin_it, end_it, 0.0) / 
      std::distance(begin_it, end_it);
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::VAR
// ----------------
// CALCULATES THE VARIANCE OF A GIVEN VECTOR
//
// IN:
//   x (VECTOR<T>&) ... VECTOR WHOSE VARIANCE IS TO BE CALCULATED
// OUT:
//   (T) VARIANCE OF x
////////////////////////////////////////////////////////////////////////////////
template<typename T>
T var(const std::vector<T> &x) {
  T m = mean(x);
  return std::inner_product(x.begin(), x.end(), x.begin(), 0.0,
      std::plus<>(), 
      [m](T const &x, T const &y){ return (x - m) * (y - m); }) 
      / (x.size() - 1);
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::COLON
// ------------------
// FUNCTION SETTING LINEARLY SPACED VALUES GIVEN STARTING POINT from AND 
// STEPSIZE diff
//
// IN:
//   vec  (VECTOR<T>&) ... VECTOR TO STORE RESULT TO
//   from (T)          ... BEGINNING VALUE
//   diff (T)          ... DIFFERENCE BETWEEN NEIGHBOURING POINTS
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
template<typename T>
void colon(std::vector<T>& vec, T from, T diff = 1) {
  vec[0] = from;
  typename std::vector<T>::iterator it;
  for (it = vec.begin() + 1; it < vec.end(); it++)
    *it = *(it - 1) + diff;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::LINSPACE
// ---------------------
// FUNCTION SETTING LINEARLY SPACED VALUES GIVEN AN STARTING POINT from AND
// AN ENDING POINT to
//
// IN:
//   vec  (VECTOR<T>&) ... VECTOR FOR STORING RESULT TO
//   from (T)          ... BEGINNING VALUE
//   to   (T)          ... ENDING VALUE
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
template<typename T>
void linspace(std::vector<T>& vec, T from, T to) {
  T diff = (to - from) / (static_cast<int>(vec.size()) - 1);
  colon<T>(vec, from, diff);
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::LOGSPACE
// ---------------------
// FUNCTION GENERATING LOGARITHMICALLY SPACED VALUES
//
// IN:
//   vec  (VECTOR<T>&) ... VECTOR FOR STORING RESULT TO
//   from (T)          ... BEGINNING VALUE
//   base (T)          ... BASE FOR LOGARITHMICAL SPACING
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
template<typename T>
void logspace(std::vector<T>& vec, T from, T base = 10) {
  vec[0] = pow(base, from);
  typename std::vector<T>::iterator it;
  for (it = vec.begin() + 1; it < vec.end(); it++)
    *it = *(it - 1) * base;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::SGN
// ----------------
// SIGNUM FUNCTION RETURNING 1 IF INPUT POSITIVE, -1 IF INPUT NEGATIVE AND
// 0 OTHERWISE. ALLOWS FOR SETTING ARBITRARY MIDPOINT
//
// IN:
//   in       (T) ... VALUE TO CHECK FOR SIGN
//   midpoint (T) ... MIDPOINT FOR COMPARISON OF SIGN (0 DEFAULT)
// OUT:
//   (INT) 1 IF in POSITIVE, -1 IF in NEGATIVE, 0 OTHERWISE
////////////////////////////////////////////////////////////////////////////////
template<typename T>
int sgn(T in, T midpoint = 0) {
  if (in > midpoint)
    return 1;
  else if (in < midpoint)
    return -1;
  else
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::LOADCSV
// --------------------
// FUNCTION WHICH LOADS A MATRIX FROM A CSV FILE TO A VECTOR OF VECTORS
//
// IN:
//   fname (STRING) ... FILENAME OF CSV FILE
//   sep   (CHAR)   ... SEPERATOR OF ELEMENTS IN A LINE
// OUT:
//   (VECTOR<VECTOR<T>>) VECTOR OF VECTORS REPRESENTING MATRIX FROM CSV FILE
////////////////////////////////////////////////////////////////////////////////
template<typename T>
std::vector<std::vector<T>> load_csv(std::string fname, char sep = ',') {
  // CREATE VECTOR OF VECTORS
  std::vector<std::vector<T>> result;
  // CREATE FILE STREAM
  std::ifstream data_file (fname);
  if (!data_file.is_open()) throw std::runtime_error("Unable to open file!");
  // CREATE TEMPORARY VARIABLE FOR STORING A SINGLE LINE
  std::string line;
  // EMPTY VALUE FOR LOADING IN VALUES
  T val;
  // READ LINES AS LONG AS ONE IS AVAILABLE
  while (std::getline(data_file, line)) {
    // CREATE STRINGSTREAM FROM THIS LINE
    std::stringstream ss (line);
    // CREATE TEMPORARY VECTOR FOR STORING VALUES OF A SINGLE LINE
    std::vector<T> tmp_result;
    // GET VALUES FROM STRINGSTREAM AS LONG AS ONE IS AVAILABLE
    while (ss >> val) {
      // APPEND VALUES TO TEMPORARY LINE VECTOR
      tmp_result.push_back(val);
      // IF A SEPERATOR IS COMING, IGNORE IT
      if (ss.peek() == sep) ss.ignore();
    }
    // PUSH COMPLETE LINE INTO RESULT VECTOR
    result.push_back(tmp_result);
  }
  // CLOSE CSV FILE
  data_file.close();
  // RETURN VECTOR OF LINES REPRESENTED BY VECTOR OF VALUES
  return result;
}

// ########################################################################## //

// MAKE USAGE OF UBLAS METHODS EASIER
using namespace boost::numeric::ublas;

////////////////////////////////////////////////////////////////////////////////
//
//  POLYNOMIAL STUFF
//  ================
//
//  ENTRIES OF POLYNOMIAL VECTORS ARE IN RISING POWER!
//  p = (a0, a1, ..., aN) ==> p(x) = a0 + a1*x + ... + aN*x^N
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::POLYFIT
// --------------------
// FITS A POLYNOMIAL OF GIVEN DEGREE TO GIVEN DATA SET
//
// IN:
//   x   (VECTOR<T>&) ... VECTOR OF X VALUES
//   y   (VECTOR<T>&) ... VECTOR OF Y VALUES
//   deg (INT)        ... DEGREE OF POLYNOMIAL TO FIT
// OUT:
//   (VECTOR<T>) VECTOR REPRESENTING POLYNOMIAL
////////////////////////////////////////////////////////////////////////////////
template<typename T>
std::vector<T> polyfit(const std::vector<T>& x, const std::vector<T>& y, 
    int deg) {
  // CHECK IF INPUT SIZES ARE EQUAL
  assert(x.size() == y.size());
  
  // GET SIZE ONCE AND STORE TO PREVENT SEVERAL CALLS TO SAME FUNCTION
  int i_size = x.size();
  // GET NUMBER OF COEFFICIENTS
  int d_size = deg + 1;
  
  // CREATE X MATRIX AND Y VECTOR
  matrix<T> X (i_size, d_size);
  matrix<T> Y (i_size, 1);
  
  // FILL X AND Y MATRICES
  for (int i = 0; i < i_size; i++) {
    Y(i, 0) = y[i];
    X(i, 0) = 1.0;
    for (int jp = 1; jp < d_size; jp++)
      X(i, jp) = X(i, jp - 1) * x[i];
  }
  
  // GET A MATRIX FROM X^T * X
  matrix<T> Xp (trans(X));
  matrix<T> A (prec_prod(Xp, X));
  // GET b VECTOR FROM X^T * Y
  matrix<T> b (prec_prod(Xp, Y));
  // USELESS?
  //std::vector<T> a (b.data().begin(), b.data().end());
  // CREATE PERMUTATION MATRIX ELEMENT
  permutation_matrix<int> perm_matr (A.size1());
  // DO LU FACTORIZATION
  const int ret_factorization = lu_factorize(A, perm_matr);
  // MAKE SURE RESULT IS NOT SINGULAR
  assert(ret_factorization == 0);
  // BACK-SUBSTITUTE RESULT FROM LU FACTORIZATION TO GET RESULT OF SLE
  lu_substitute(A, perm_matr, b);
  
  // WRAP RESULT IN VECTOR AND RETURN IT
  return std::vector<T>(b.data().begin(), b.data().end());  
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::POYVAL
// -------------------
// EVALUATE A GIVEN POLYNOMIAL p AT A GIVEN POINT x
//
// IN:
//   p (VECTOR<T>&) ... POLYNOMIAL
//   x (T)         ... POINT WHERE p SHOULD BE EVALUATED
// OUT:
//   (T) EXPRESSION p(x); POLYNOMIAL p EVALUATED AT POINT x
////////////////////////////////////////////////////////////////////////////////
template<typename T>
T polyval(const std::vector<T>& p, const T x) {
  T expx = 1;
  T result = 0;
  for (auto& pi : p) {
    result += pi * expx;
    expx *= x;
  }
  return result;
}

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::POLYVAL
// --------------------
// EVALUATE POLYNOMIAL AT A BUNCH OF GIVEN VALUES
//
// IN:
//   res (VECTOR<T>&) ... EMPTY (SIZE 0) VECTOR WHERE RESULTS SHOULD BE STORED
//   p   (VECTOR<T>&) ... POLYNOMIAL
//   x   (VECTOR<T>&) ... VECTOR OF VALUES WHERE POLYNOMIAL SHOULD BE EVALUATED
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
template<typename T>
void polyval(std::vector<T>& res, const std::vector<T>& p, const std::vector<T>& x) {
  for (auto& xval : x)
    res.push_back(polyval<T>(p, xval));
}

// ########################################################################## //

////////////////////////////////////////////////////////////////////////////////
// VECTORUTILS::SOLVE_TRIDIAG
// --------------------------
// SOLVE A TRIDIAGONAL SYSTEM OF LINEAR EQUATIONS
//
// IN:
//   a (VECTOR<T>&) ... LOWER SIDE DIAGONAL
//   b (VECTOR<T>&) ... MAIN DIAGONAL
//   c (VECTOR<T>&) ... UPPER SIDE DIAGONAL
//   r (VECTOR<T>&) ... INHOMOGENEITY VECTOR
// OUT:
//   (VECTOR<T>) SOLUTION VECTOR x
////////////////////////////////////////////////////////////////////////////////
template<typename T>
std::vector<T> solve_tridiag(const std::vector<T>& a, const std::vector<T>& b, 
    const std::vector<T>& c, const std::vector<T>& r) {
  // CALCULATE SIZE ONCE AND STORE FOR REUSE
  long unsigned n = r.size();
  // CHECK IF DIAGONALS HAVE CORRECT SIZES
  assert(c.size() == n - 1);
  assert(b.size() == n);
  assert(a.size() == n - 1);
  // CREATE RHO, BETA AND Z VECTORS
  std::vector<T> rho {r[0]};
  std::vector<T> beta {b[0]};
  std::vector<T> z (n);
  // CALCULATE BETA AND RHO FORWARD ITERATIVELY
  for (long unsigned i = 1; i < n; i++) {
    beta.push_back(b[i] - a[i] / beta.back() * c[i - 1]);
    rho.push_back(r[i] - a[i] / beta[i - 1] * rho.back());
  }
  // SET LAST Z VALUE
  z.back() = rho.back() / beta.back();
  // CALCULATE Z BACKWARD ITERATIVELY
  for (long unsigned j = n - 1; j >= 1; j--)
    z[j - 1] = (rho[j - 1] - c[j - 1] * z[j]) / beta[j - 1];
  // RETURN Z VECTOR
  return z;
}

} /* NAMESPACE VECTORUTILS */

#endif /* VECTORUTILS_H */
