////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  FOURVECTOR.H
////  ============
////
////  HEADER FILE ALLOWING FOR CREATION OF PRIMITIVE FOURVECTORS.
////  AT THE MOMENT THE ONLY DEFINED OPERATION IS THE EUCLIDEAN SCALAR PRODUCT.
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef FOURVECTOR_H
#define FOURVECTOR_H

////////////////////////////////////////////////////////////////////////////////
// 
// FOURVECTOR CLASS
// ================
//
// CLASS IMPLEMENTING PRIMITIVE FOURVECTORS.
// ELEMENTS CAN BE ACCESSED VIA [] OPERATOR AND USING THE * OPERATOR, THE 
// EUCLIDEAN SCALAR PRODUCT BETWEEN TWO FOURVECTORS CAN BA CALCULATED.
//
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class FourVector {
private:
    /* THE 4 ELEMENTS OF THE FOURVECTOR */
    T elements [4];
public:
    ////////////////////////////////////////////////////////////////////////
    // FOURVECTOR::FOURVECTOR
    // ----------------------
    // CONSTRUCTOR FOR CREATING AN EMPTY FOURVECTOR (ALL ELEMENTS ARE SET 0).
    //
    // IN:
    //   -
    // OUT:
    //   (FOURVECTOR)
    ////////////////////////////////////////////////////////////////////////
    FourVector() : elements{0.0, 0.0, 0.0, 0.0} {}

    ////////////////////////////////////////////////////////////////////////
    // FOURVECTOR::FOURVECTOR
    // ----------------------
    // CONSTRUCTOR FOR CREATING A FOURVECTOR WITH GIVEN ENTRIES.
    //
    // IN:
    //   eX (T) ... ENTRY AT POSITION X (X = 0, 1, 2, 3)
    // OUT:
    //   (FOURVECTOR)
    ////////////////////////////////////////////////////////////////////////
    FourVector(T e0, T e1, T e2, T e3) : elements {e0, e1, e2, e3} {}
    
    ////////////////////////////////////////////////////////////////////////
    // FOURVECTOR::OPERATOR[]
    // ----------------------
    // OVERLOADED ACCESS OPERATOR TO GET MUTABLE REFERENCE TO j-TH ELEMENT.
    //
    // IN:
    //   j (INT) ... INDEX OF ELEMENT ONE WANTS TO GET
    // OUT:
    //   (T&)
    ////////////////////////////////////////////////////////////////////////
    T& operator[](int j) {
        return elements[j];
    }

    ////////////////////////////////////////////////////////////////////////
    // FOURVECTOR::OPERATOR[]
    // ----------------------
    // OVERLOADED ACCESS OPERATOR TO GET CONST REFERENCE TO j-TH ELEMENT.
    //
    // IN:
    //   j (INT) ... INDEX OF ELEMENT ONE WANTS TO GET
    // OUT:
    //   (T&)
    ////////////////////////////////////////////////////////////////////////
    const T& operator[](int j) const {
        return elements[j];
    }

    ////////////////////////////////////////////////////////////////////////
    // FOURVECTOR::OPERATOR*
    // ---------------------
    // OVERLOADED MULTIPLICATION OPERATOR FOR EUCLIDEAN SCALAR PRODUCT
    // BETWEEN TWO FOURVECTORS.
    //
    // IN:
    //   lhs (CONST FOURVECTOR<T>&) ... LEFT FOURVECTOR AS FACTOR
    //   rhs (CONST FOURVECTOR<T>&) ... RIGHT FOURVECTOR AS FACTOR
    // OUT:
    //   (T) EUCLIDEAN SCALAR PRODUCT BETWEEN lhs AND rhs
    ////////////////////////////////////////////////////////////////////////
    friend T operator*(const FourVector<T>& lhs, const FourVector<T>& rhs) {
        T sum = T(0);
        for (int j = 0; j < 4; j++)
            sum += lhs.elements[j] * rhs.elements[j];
        return sum;
    }
};

#endif /* FOURVECTOR_H */