////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  DIRACMATRIX.H
////  =============
////
////  HEADER FILE EXTENDING THE STMATRIX CLASS. CREATES A SUBCLASS DERIVED FROM
////  THE STMATRIX CLASS THAT DEALS WITH DIRAC (GAMMA) MATRICES IN 
////  EUCLIDEAN SPACE.
////
////  IF ONE ONLY NEEDS CONST& TO A SPECIFIC DIRAC MATRIX, ONE DOES NOT NEED
////  TO CREATE IT. INSTEAD, ONE CAN ACCESS THE ONES ALREADY STORED INSIDE
////  dirac_matrices CONTAINER OR dirac_fourvector IN CASE A FOURVECTOR OF 
////  THE FIRST 4 DIRAC MATRICES (1-4) IS NEEDED.
////
////  USAGE FOR GETTING THE 5TH GAMMA MATRIX AND GETTING A DIRAC FOURVECTOR:
////
////  STMatrix<C> gamma_5 = dirac_matrices[5];
////  FourVector<DiracMatrix<C>> gamma_vector = dirac_fourvector;
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef DIRACMATRIX_H
#define DIRACMATRIX_H

/* DIRAC MATRICES ARE COMPLEX. STL COMPLEX CLASS IS DEFAULT COMPLEX DATA TYPE */
#include <complex>
/* FOR ERROR MESSAGES */
#include <iostream>
/* FOR STORING THE DIRAC MATRICES INSIDE THE dirac_matrices CONTAINER */
#include <vector>

/* DIRAC MATRICES ARE A KIND OF SPACETIME MATRIX -> THEY INHERIT FROM THEM */
#include "stmatrix.h"
/* FOR THE SLASH METHOD AND THE DIRAC VECTOR */
#include "fourvector.h"

////////////////////////////////////////////////////////////////////////////////
// 
// DIRACMATRIX CLASS : STMATRIX
// ============================
//
// CLASS IMPLEMENTING THE DIRAC MATRICES (ALSO KNOWN AS GAMMA MATRICES).
// THESE MATRICES WILL BE CREATED WITH THE ENTRIES THEY HAVE IN EUCLIDEAN SPACE.
//
// NORMALLY ONE SHOULD NEVER NEED TO DIRECTLY INSTANTIATE OBJECTS FROM THIS 
// CLASS AS THESE ARE CONSTANT MATRICES THAT NEVER NEED TO BE MUTATED.
// TO GET INSTANCES OF THIS CLASS SIMPLY GET A CONST& FROM THE PROVIDED 
// dirac_matrices CONTAINER OR THE dirac_fourvector.
//
////////////////////////////////////////////////////////////////////////////////
template<typename C = std::complex<double>>
class DiracMatrix : public STMatrix<C> {
public:
    ////////////////////////////////////////////////////////////////////////
    // DIRACMATRIX::DIRACMATRIX
    // ------------------------
    // CONSTRUCTOR FOR CREATING A DIRACMATRIX FROM A GIVEN SET OF ELEMENTS.
    //
    // IN:
    //   eX (C) ... ELEMENT FOR INDEX X (X = 0, ..., 15)
    // OUT:
    //   (DIRACMATRIX)
    ////////////////////////////////////////////////////////////////////////
    DiracMatrix(C e0, C e1, C e2, C e3,
                C e4, C e5, C e6, C e7,
                C e8, C e9, C e10, C e11,
                C e12, C e13, C e14, C e15) {
        /* SET ALL ELEMENTS TO THE GIVEN VALUE */
        this->setData( e0,  e1,  e2,  e3,
                 e4,  e5,  e6,  e7,
                 e8,  e9, e10, e11,
                e12, e13, e14, e15);
    }

    ////////////////////////////////////////////////////////////////////////
    // DIRACMATRIX::DIRACMATRIX
    // ------------------------
    // CONSTRUCTOR FOR CREATING A SPECIFIC DIRAC MATRIX.
    //
    // IN:
    //   j (INT) ... INDEX OF DIRAC MATRIX (j = 1, 2, 3, 4, 5)
    // OUT:
    //   (DIRACMATRIX)
    ////////////////////////////////////////////////////////////////////////
    DiracMatrix(int j) : STMatrix<C>() {
        /* DEPENDING ON j CREATE SPECIFIC DIRAC MATRIX */
        switch (j) {
        case 1: /* GAMMA_1 */
            this->setData( 0.0 ,  0.0 ,  0.0 , -1.0i,
                           0.0 ,  0.0 , -1.0i,  0.0 ,
                           0.0 ,  1.0i,  0.0 ,  0.0 ,
                           1.0i,  0.0 ,  0.0 ,  0.0 );
            break;
        case 2: /* GAMMA_2 */
            this->setData( 0.0 ,  0.0 ,  0.0 , -1.0 ,
                           0.0 ,  0.0 ,  1.0 ,  0.0 ,
                           0.0 ,  1.0 ,  0.0 ,  0.0 ,
                          -1.0 ,  0.0 ,  0.0 ,  0.0 );
            break;
        case 3: /* GAMMA_3 */
            this->setData( 0.0 ,  0.0 , -1.0i,  0.0 ,
                           0.0 ,  0.0 ,  0.0 ,  1.0i,
                           1.0i,  0.0 ,  0.0 ,  0.0 ,
                           0.0 , -1.0i,  0.0 ,  0.0 );
            break;
        case 4: /* GAMMA_4 */
            this->setData( 1.0 ,  0.0 ,  0.0 ,  0.0 ,
                           0.0 ,  1.0 ,  0.0 ,  0.0 ,
                           0.0 ,  0.0 , -1.0 ,  0.0 ,
                           0.0 ,  0.0 ,  0.0 , -1.0 );
            break;
        case 5: /* GAMMA_5 */
            this->setData( 0.0 ,  0.0 ,  0.0 , -1.0 ,
                           0.0 ,  0.0 , -1.0 ,  0.0 ,
                           0.0 , -1.0 ,  0.0 ,  0.0 ,
                          -1.0 ,  0.0 ,  0.0 ,  0.0 );
            break;
        default: /* IN CASE AN INVALID j IS PASSED */
            std::cerr << "No valid index given to Dirac Initializer: " << 
                    j << "\n";
            break;
        }
    }

    ////////////////////////////////////////////////////////////////////////
    // DIRACMATRIX::SLASH
    // ------------------
    // STATIC METHOD WHICH SLASHES A GIVEN FOURVECTOR.
    // CONCRETELY IT USES THE dirac_fourvector DEFINED BELOW TO MULTIPLY
    // THE J-TH ELEMENT OF THE FOURVECTOR WITH THE J-TH DIRAC MATRIX AND 
    // SUM ALL THOSE RESULTING MATRICES UP. THE RESULT IS RETURNED AS A
    // STMATRIX ELEMENT.
    //
    // IN:
    //   vec (CONST FOURVECTOR<T>) ... VECTOR TO SLASH
    // OUT:
    //   (STMATRIX<T>) RESULTING MATRIX FROM SLASHING vec
    ////////////////////////////////////////////////////////////////////////
    template<typename T>
    static STMatrix<C> slash(const FourVector<T> vec);
};

/* A FOURVECTOR CONTAINING THE FIRST 4 DIRAC MATRICES */
const FourVector<DiracMatrix<std::complex<double>>> dirac_fourvector 
        {1, 2, 3, 4};

/* A CONTAINER FOR ACCESSING ALL DIRAC MATRICES */
/* DIRAC MATRICES SHOULD BE GOTTEN FROM THIS INSTEAD OF EACH TIME 
   CREATING A NEW ONE */
class DiracMatrixContainer {
    /* STORE MATRICES INTERNALLY IN STD::VECTOR */
    const std::vector<DiracMatrix<std::complex<double>>> dirac_matrix_vector 
            {1, 2, 3, 4, 5};
public:
    /* OVERLOADED [] OPERATOR FOR ACCESSING THE j-TH DIRAC MATRIX */
    const DiracMatrix<std::complex<double>>& operator[](int j) const {
        return dirac_matrix_vector[j - 1];
    }
} dirac_matrices;

/* FOR INFORMATION SEE DECLARATION ABOVE IN CLASS DEFINITION */
template<typename C>
template<typename T>
STMatrix<C> DiracMatrix<C>::slash(const FourVector<T> vec) {
    /* DECLARE RESULT MATRIX */
    STMatrix<C> result;
    for (int j = 0; j < 4; j++) {
        /* ADD j-TH DIRAC MATRIX MULTIPLIED BY j-TH ELEMENT OF vec */
        result += dirac_fourvector[j].scale(vec[j]);
    }
    /* RETURN RESULTING STMATRIX */
    return result;
}

#endif /* DIRACMATRIX_H */