////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  STMATRIX.H
////  ==========
////
////  HEADER FILE FOR IMPLEMENTING SPACETIME (4x4) MATRICES.
////  TO INCREASE THE SPEED OF THIS MODULE, NO STL CONTAINER IS USED TO STORE
////  THE ELEMENTS OF SUCH A MATRIX. THEY ARE STORED IN A FIXED SIZE C-STYLE 
////  ARRAY.
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef STMATRIX_H
#define STMATRIX_H

/* DEFAULT DATA STRUCTURE FOR COMPLEX NUMBERS */
#include <complex>
/* FOR PRINTING THE MATRIX TO STDOUT */
#include <iostream>

/* TO BE ABLE TO USE THE IMAGINARY UNIT i WITHOUT PREFIXES */
using namespace std::complex_literals;

////////////////////////////////////////////////////////////////////////////////
// 
// STMATRIX CLASS
// ==============
//
// CLASS DESCRIBING A GENERAL 4x4 MATRIX.
// 
////////////////////////////////////////////////////////////////////////////////
template<typename C = std::complex<double>>
class STMatrix {
private:
    /* ELEMENTS OF THE MATRIX */
    C elements[16];

public:
    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::STMATRIX
    // ------------------
    // CONSTRUCTOR FOR CREATING A STMATRIX WITH ALL ELEMENTS BEING 0.
    //
    // IN:
    //   -
    // OUT:
    //   (STMATRIX)
    ////////////////////////////////////////////////////////////////////////
    STMatrix() {
        for (auto& e : elements)
            e = C(0);
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::STMATRIX
    // ------------------
    // CONSTRUCTOR FOR CREATING A STMATRIX GIVEN ALL ELEMENTS AS A FIXED
    // SIZE C-STYLE ARRAY.
    //
    // IN:
    //   elem_ (C[16]) ... FIXED SIZE C-STYLE ARRAY CONTAINING ELEMENTS
    // OUT:
    //   (STMATRIX)
    ////////////////////////////////////////////////////////////////////////
    STMatrix(C elem_[16]) {
        for (int j = 0; j < 16; j++)
            elements[j] = elem_[j];
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::SETUNITMATRIX
    // -----------------------
    // METHOD SETTING ALL ELEMENTS OF THE CURRENT MATRIX TO BE EQUIVALENT
    // TO A UNIT MATRIX.
    //
    // IN:
    //   -
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////
    void setUnitMatrix() {
        for (int j = 0; j < 16; j++)
            elements[j] = (j % 5 == 0) ? 1.0 : 0.0;
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::SCALE
    // ---------------
    // METHOD MULTIPLYING EVERY ELEMENT OF THE MATRIX BY A GIVEN VALUE.
    //
    // IN:
    //   scale_by (CONST T) ... NUMBER TO SCALE EVERY ELEMENT BY
    // OUT:
    //   (STMATRIX<C>) REFERENCE TO *this
    ////////////////////////////////////////////////////////////////////////
    template<typename T>
    STMatrix<C> scale (const T scale_by) const {
        STMatrix<C> tmp = *this;
        for (auto& r : tmp.elements)
            r *= scale_by;
        return tmp;
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::SCALE
    // ---------------
    // METHOD MULTIPLYING EVERY ELEMENT OF THE MATRIX BY A GIVEN VALUE.
    //
    // IN:
    //   scale_by (CONST T) ... NUMBER TO SCALE EVERY ELEMENT BY
    // OUT:
    //   (STMATRIX<C>&) REFERENCE TO *this
    ////////////////////////////////////////////////////////////////////////
    template<typename T>
    STMatrix<C>& scale (const T scale_by) {
        for (auto& r : this->elements)
            r *= scale_by;
        return *this;
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::OPERATOR+=
    // --------------------
    // OVERLOADED += OPERATOR TO ADD TWO MATRICES.
    //
    // IN:
    //   other (CONST STMATRIX<C>&) ... REFERENCE TO SECOND SUMMAND STMATRIX
    // OUT:
    //   (STMATRIX<C>&) REFERENCE TO *this AFTER ADDING other
    ////////////////////////////////////////////////////////////////////////
    STMatrix<C>& operator+=(const STMatrix<C>& other) {
        for (int j = 0; j < 16; j++)
            this->elements[j] += other.elements[j];
        return *this;
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::OPERATOR*=
    // --------------------
    // OVERLOADED *= OPERATOR TO DO NORMAL MATRIX MULTIPLICATION.
    //
    // IN:
    //   other (CONST STMATRIX<C>&) ... MATRIX TO MULTIPLY FROM THE RIGHT
    // OUT:
    //   (STMATRIX<C>&) REFERENCE TO *this AFTER DOING MATRIX MULTIPLICATION
    ////////////////////////////////////////////////////////////////////////
    STMatrix<C>& operator*=(const STMatrix<C>& other) {
        STMatrix<C> tmp = *this * other;
        this->setData(tmp);
        return *this;
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::PRINT
    // ---------------
    // METHOD WHICH PRINTS THE MATRIX TO STDOUT.
    //
    // IN:
    //   -
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////
    void print() const {
        std::cout << "   ";
        for (int j = 0; j < 16; j++) {
            std::cout << elements[j];
            if (j != 15) {
                std::cout << ", ";
                if (j % 4 == 3)
                    std::cout << "\n   ";
            }
        }
        std::cout << "\n";
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::SETDATA
    // -----------------
    // METHOD WHICH SETS THE ELEMENTS GIVEN AS INDIVIDUAL ARGUMENTS AS THE
    // ENTRIES IN this
    //
    // IN:
    //   eX (C) ... VALUE TO STORE ON POSITION X (X = 0, ..., 15)
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////
    void setData(C e0, C e1, C e2, C e3,
                 C e4, C e5, C e6, C e7,
                 C e8, C e9, C e10, C e11,
                 C e12, C e13, C e14, C e15) {
        this->elements[0] = e0;
        this->elements[1] = e1;
        this->elements[2] = e2;
        this->elements[3] = e3;
        this->elements[4] = e4;
        this->elements[5] = e5;
        this->elements[6] = e6;
        this->elements[7] = e7;
        this->elements[8] = e8;
        this->elements[9] = e9;
        this->elements[10] = e10;
        this->elements[11] = e11;
        this->elements[12] = e12;
        this->elements[13] = e13;
        this->elements[14] = e14;
        this->elements[15] = e15;
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::SETDATA
    // -----------------
    // METHOD WHICH COPIES THE VALUES OF A GIVEN MATRIX INTO ITSELF.
    //
    // IN:
    //   other (CONST STMATRIX<C>&) ... MATRIX TO STEAL ELEMENTS FROM
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////
    void setData(const STMatrix<C>& other) {
        for (int j = 0; j < 16; j++)
            this->elements[j] = other.elements[j];
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::AT
    // ------------
    // FUNCTION TO ACCESS ELEMENTS TO MODIFY USING ROW AND COLUMN INDICES.
    //
    // IN:
    //   i (CONST INT) ... ROW INDEX
    //   j (CONST INT) ... COLUMN INDEX
    // OUT:
    //   (C&) REFERENCE TO ELEMENT AT POSITION (i, j) OF *this
    ////////////////////////////////////////////////////////////////////////
    C& at(const int i, const int j) {
        return this->elements[i * 4 + j];
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::AT
    // ------------
    // FUNCTION TO ACCESS ELEMENTS TO READ USING ROW AND COLUMN INDICES.
    //
    // IN:
    //   i (CONST INT) ... ROW INDEX
    //   j (CONST INT) ... COLUMN INDEX
    // OUT:
    //   (C) COPY OF ELEMENT AT POSITION (i, j) OF *this
    ////////////////////////////////////////////////////////////////////////
    C at(const int i, const int j) const {
        return this->elements[i * 4 + j];
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::OPERATOR()
    // --------------------
    // OVERLOADED FUNCTION CALL OPERATOR () TO ACCESS ELEMENTS TO MODIFY 
    // USING ROW AND COLUMN INDICES.
    //
    // IN:
    //   i (CONST INT) ... ROW INDEX
    //   j (CONST INT) ... COLUMN INDEX
    // OUT:
    //   (C&) REFERENCE TO ELEMENT AT POSITION (i, j) OF *this
    ////////////////////////////////////////////////////////////////////////
    C& operator()(const int i, const int j) {
        return this->elements[i * 4 + j];
    }

    ////////////////////////////////////////////////////////////////////////
    // STMATRIX::OPERATOR()
    // --------------------
    // OVERLOADED FUNCTION CALL OPERATOR () TO ACCESS ELEMENTS TO READ 
    // USING ROW AND COLUMN INDICES.
    //
    // IN:
    //   i (CONST INT) ... ROW INDEX
    //   j (CONST INT) ... COLUMN INDEX
    // OUT:
    //   (C) COPY OF ELEMENT AT POSITION (i, j) OF *this
    ////////////////////////////////////////////////////////////////////////
    C operator()(const int i, const int j) const {
        return this->elements[i * 4 + j];
    }
};

////////////////////////////////////////////////////////////////////////
// STMATRIX::OPERATOR+
// -------------------
// FUNCTION THAT OVERLOADS THE + OPERATOR TO ADD TWO STMATRICES
//
// IN:
//   lhs (CONST STMATRIX<C>&) ... LEFT SUMMAND
//   rhs (CONST STMATRIX<C>&) ... RIGHT SUMMAND
// OUT:
//   (STMatrix<C>) SUM OF lhs AND rhs
////////////////////////////////////////////////////////////////////////
template<typename C>
STMatrix<C> operator+ (const STMatrix<C>& lhs, const STMatrix<C>& rhs) {
    STMatrix<C> tmp = lhs;
    return tmp += rhs;
}

////////////////////////////////////////////////////////////////////////
// STMATRIX::OPERATOR*
// -------------------
// FUNCTION THAT OVERLOADS THE * OPERATOR TO PERFORM MATRIX MULTIPLICATION
//
// IN:
//   lhs (CONST STMATRIX<C>&) ... LEFT FACTOR
//   rhs (CONST STMATRIX<C>&) ... RIGHT FACTOR
// OUT:
//   (STMatrix<C>) MATRIX PRODUCT OF lhs AND rhs
////////////////////////////////////////////////////////////////////////
template<typename C>
STMatrix<C> operator*(const STMatrix<C>& lhs, const STMatrix<C>& rhs) {
    C tmp;
    STMatrix<C> res;
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            tmp = 0;
            for (int j = 0; j < 4; j++) {
                tmp += lhs.at(row, j) * rhs.at(j, col);
            }
            res.at(row, col) = tmp;
        }
    }
    return res;
}

#endif /* STMATRIX_H */